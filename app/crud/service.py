from sqlalchemy import func
from sqlalchemy.engine.row import Row
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.future import select
from sqlalchemy.sql.elements import and_

from app.crud.base import CRUDBase
from app.db.base import Service
from app.schemas import ServiceCreate, ServiceDetail, ServiceUpdate


class ServiceCRUD(CRUDBase[ServiceDetail, ServiceCreate, ServiceUpdate]):
    async def get_group_by_name_stats(self, db: AsyncSession, name: str) -> list[Row]:
        select_st = (
            select(
                Service.name,
                Service.path,
                func.min(Service.response_time_ms),
                func.max(Service.response_time_ms),
                func.avg(Service.response_time_ms),
                func.percentile_cont(0.99).within_group(Service.response_time_ms).label("p99"),
            )
            .where(and_(Service.name == name))
            .group_by(Service.path, Service.name)
        )
        res = await db.execute(select_st)
        return res.fetchall()


service = ServiceCRUD(Service)
