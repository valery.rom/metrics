from sqlalchemy import BigInteger, Column, Integer, String

from app.db.base_class import Base


class Service(Base):
    __tablename__ = "service"

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String, nullable=False, index=True)
    path = Column(String, nullable=False, index=True)
    response_time_ms = Column(BigInteger, nullable=False)
