from typing import Dict, Optional

from fastapi import APIRouter, Depends, Request
from fastapi.security import HTTPBearer
from sqlalchemy import and_
from sqlalchemy.ext.asyncio import AsyncSession

from app import crud
from app.api.deps import get_db
from app.db.base import Base, Service
from app.schemas import (
    ServiceCreate,
    ServiceDetail,
    ServiceGroupByPathStatsDetail,
    ServicePathMetricList,
    ServiceUpdate,
)

router = APIRouter()


@router.post("/metrics", status_code=201, response_model=ServiceDetail)
async def create(service_in: ServiceCreate, db: AsyncSession = Depends(get_db)):
    return await crud.service.create(db, service_in)


@router.get("/metrics", status_code=200, response_model=list[ServicePathMetricList])
async def get_multi(
    db: AsyncSession = Depends(get_db),
):
    return await crud.service.get_multi(db)


@router.delete("/metrics/{id}", status_code=204)
async def remove(id: int, db: AsyncSession = Depends(get_db)):
    return await crud.service.remove(db, filter_expr=and_(Service.id == id))


@router.get("/metrics/{service_name}", response_model=list[ServiceGroupByPathStatsDetail])
async def get_service_group_by_name_stats(service_name: str, db: AsyncSession = Depends(get_db)):
    return await crud.service.get_group_by_name_stats(db, service_name)
