import logging.config

import uvicorn
from fastapi import FastAPI

from app.api.v1.api import router
from app.config.settings import settings
from app.db.session import db_interface

logging.config.dictConfig(settings.logging)
logger = logging.getLogger("app")

app = FastAPI(title=settings.PROJECT_NAME, root_path=settings.ROOT_PATH)

app.include_router(router)


@app.on_event("shutdown")
async def shutdown():
    await db_interface.close()


if __name__ == "__main__":
    uvicorn.run("app.main:app", host="0.0.0.0", port=8000, reload=True)
