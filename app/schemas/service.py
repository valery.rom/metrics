from __future__ import annotations

from pydantic import BaseModel


class ServiceBase(BaseModel):
    response_time_ms: int
    name: str
    path: str


class ServiceCreate(ServiceBase):
    ...


class ServiceUpdate(ServiceBase):
    ...


class ServiceDetail(ServiceBase):
    id: int

    class Config:
        orm_mode = True


class ServicePathMetricList(ServiceBase):
    id: int

    class Config:
        orm_mode = True


class ServiceGroupByPathStatsDetail(BaseModel):
    path: str
    avg: float
    min: int
    max: int
    p99: float

    class Config:
        orm_mode = True
