from pathlib import Path
from typing import Any, Dict

from pydantic import BaseSettings, Field, PostgresDsn, validator

ROOT_DIR = Path(__file__).resolve(strict=True).parent.parent.parent


class CommonSettings(BaseSettings):
    DEBUG: bool = True
    ENV_STATE: str | None = Field(None, env="ENV_STATE")

    SERVER_NAME: str = "Metrics Server"
    API_URL: str = "http://0.0.0.0:8000"
    ROOT_PATH: str = ""

    PROJECT_NAME: str = "Metrics"

    POSTGRES_HOST: str
    POSTGRES_PORT: str
    POSTGRES_USER: str
    POSTGRES_PASSWORD: str
    POSTGRES_DB: str
    SQLALCHEMY_DATABASE_URI: PostgresDsn | None = None

    @staticmethod
    def get_db_kwargs(values: Dict[str, Any]):
        return dict(
            user=values.get("POSTGRES_USER"),
            password=values.get("POSTGRES_PASSWORD"),
            host=values.get("POSTGRES_HOST"),
            port=values.get("POSTGRES_PORT"),
            path=f"/{values.get('POSTGRES_DB') or ''}",
        )

    @validator("SQLALCHEMY_DATABASE_URI", pre=True)
    def assemble_db_connection(cls, v: str | None, values: Dict[str, Any]) -> Any:
        if isinstance(v, str):
            return v
        return PostgresDsn.build(scheme="postgresql+asyncpg", **cls.get_db_kwargs(values))

    # Logging
    logging: dict = {
        "version": 1,
        "disable_existing_loggers": False,
        "formatters": {
            "console": {
                "format": ("%(name)-12s %(asctime)s %(levelname)-8s " "%(filename)s:%(funcName)s %(message)s"),
                "datefmt": "%d.%m.%y %H:%M:%S",
            }
        },
        "handlers": {
            "console": {
                "class": "logging.StreamHandler",
                "level": "DEBUG",
                "formatter": "console",
                "stream": "ext://sys.stdout",
            }
        },
        "loggers": {
            "app": {"level": "DEBUG", "handlers": ["console"]},
        },
    }

    class Config:
        env_file = str(ROOT_DIR / "app" / "config" / ".env")


class LocalSettings(CommonSettings):
    ...


class StageSettings(CommonSettings):
    ...


class ProdSettings(CommonSettings):
    DEBUG: bool = False


settings_index = dict(
    local=LocalSettings,
    stage=StageSettings,
    prod=ProdSettings,
)

settings = settings_index.get(CommonSettings().ENV_STATE, LocalSettings)()
