# Metrics

Metrics Server description

## Системные требования

- git
- Docker
- Docker Compose 

## Локальная разработка

1. Добавить переменные окружения в файл ./app/config/.env по примеру из ./app/config/example.env

2. Запуск проекта

```shell
# Создать, запустить.
docker compose up --build

# Остановить
ctrl + C

# Удалить контейнеры
docker compose down

# Запустить
docker compose up

# Отобразить работающие контейнеры
docker ps

# Запуск в контейнере bash shell
docker exec -it <container name> bash
```

3. Работа с проектом в интерактивная документация апи 

- добавление метрик - http://0.0.0.0:8000/docs#/default/create_metrics_post

- отображение статистики по сервису, сгруппированных по пути запроса - http://0.0.0.0:8000/docs#/default/get_service_group_by_name_stats_metrics__service_name__get
